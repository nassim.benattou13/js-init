import { Builder } from "../_helpers/builder.class";
import { Product } from "./product.class";
import { NameIsEmptyError, IdIsEmptyError, StockNullError, StockNegativeError } from "./product-error.class";

export class ProductBuilder extends Builder{
    /**
     * ID attribute for the new Product
     * @var string
     */
    id = ''

    /**
     * Name of the new Product
     * @var string
     */
    name = ''

    /**
     * Stock of the new Product
     */
    stock = 0

    /**
     * Build a concrete Product
     * Throws exceptions
     * @returns Product
     * @see Builder
     * @override
     */
    build(){
        if(this.id === ''){
            throw new IdIsEmptyError(`Cannot build Product without an ID`)
        }

        if(this.name === ''){
            throw new NameIsEmptyError(`Cannot build Product without a Name`)
        }

        if(isNaN(this.stock) || this.stock < 0){
            throw new StockNegativeError(`Stock cannot be negative and must be numeric`)
        }

        if(this.stock === null){
            throw new StockNullError(`Stock cannot be null`)
        }

        const product = Product.getInstance()
        product.id = this.id
        product.name = this.name
        product.stock = this.stock

        return product
    }
}