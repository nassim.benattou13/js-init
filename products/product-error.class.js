export class NameIsEmptyError extends Error{
    constructor(message) {
        super(message);
        this.name = "NameIsEmptyError"
        this.status = "400"
    }
}

export class IdIsEmptyError extends Error{
    constructor(message) {
        super(message);
        this.name = "IdIsEmptyError"
        this.status = "400"
    }
}

export class StockNullError extends Error{
    constructor(message) {
        super(message);
        this.name = "StockNullError"
        this.status = "400"
    }
}

export class StockNegativeError extends Error{
    constructor(message) {
        super(message);
        this.name = "StockNegativeError"
        this.status = "400"
    } 
}