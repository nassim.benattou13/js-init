require('jest-sorted')
const {usingArrays, objectArray} = require('./../index.js')

describe(`Array manipulation`, () => {
    let array

    beforeEach(() => {
        array = usingArrays()
    })

    it(`Should have 8 items`, () => {
        expect(array.length).toBe(8)
    })

    it(`Should have 3 even values`, () => {
        const expectedEvenCount = 3
        const sortArray = array.filter((num) => num % 2 === 0).length
        expect(sortArray).toEqual(expectedEvenCount)
    })

    it(`Should have a sum of '87`, () => {
        // Use a reducer
        const sum = array.reduce((acc, current) => acc + current, 0)
        expect(sum).toBe(87)
    })

    it(`Should have an odd sum of '43'`, () => {
        // Using filter and reduce
        const oddSum = array
            .filter((value) => value % 2 !== 0)
            .reduce((acc, current) => acc + current, 0)
        expect(oddSum).toBe(43)
    })

    it(`Should have a global stock of 21`, () => {
        // map + reduce
        const stock = objectArray
            .map((value) => value.stock)
            .reduce((acc, current) => acc + current, 0)

        expect(stock).toBe(21)
    })

    it(`Should be sorted by id acending`, () => {
        // sort + localeCompare
        const sortedId = objectArray
            .map((value) => value.id)
            .sort((a, b) => a.localeCompare(b))
        
        expect(sortedId).toBeSorted({ ascending: true })
    })

    it(`Should be sorted by name descending`, () => {
        // sort + localeCompare
        const sortedName = objectArray
            .map((value) => value.name)
            .sort((a, b) => b.localeCompare(a))
        
        expect(sortedName).toBeSorted({ descending: true })
    })

    it(`Should give an array of product with a stock gt 5`, () => {
        // Original array must be mutated
        const workArray = [ ... objectArray]

        for(let i = 0; i < workArray.length; i++){
            if(workArray[i].stock < 5){
                objectArray.splice(i, 1)
            }
        }

        expect(objectArray.length).toBe(2)
    })
})