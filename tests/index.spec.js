const {primitiveAffectation, myBirthDate, birthDate, objectCopy, usingPrototypePattern} = require('./../index')

describe(`index.js`, () => {
    it(`Should return 'Benattou'`, () => {
        expect(primitiveAffectation()).toBe('Benattou')
    })

    // Testing references
    it(`Should have the same reference`, () => {
        expect(birthDate).toStrictEqual(myBirthDate)
        
    })

    it(`Should have different reference`, () => {
        const theDate = new Date(1998, 4, 13)
        const otherDate = objectCopy(theDate)

        expect(theDate === otherDate).toBeFalsy()

        const protoDate = usingPrototypePattern(theDate)

        expect(theDate === protoDate).toBeFalsy()
    })

    it(`Should raised an exception if a Date is not passed `, () => {
        expect(() => usingPrototypePattern('Dummy things'))
    })
})